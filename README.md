# SystemC course
# Lesson 0A

The goal of the course is to teach you utilizing SystemC
for event-driven, real-time programming.
The language itself was developed primarily for designing
electronic systems, up to system level (hence its name),
it can be advantageously utilized for understanding the principles
of event-driven programming, real time caveats, and many other things.
The course will  provide branches to many prossible application fields.
The way is just to give examples, source codes, tasks and excercises
As SystemC (from programming point of view) is a C++ library,
we will start be brushing up your C++ programming knowledge.

# The needed knowledge
Of course, advanced  level programming and practice C++ is assumed.
The course can be downloaded from a repository, i.e. some elementary
knowledge of git is necessary.
Especially in the second half of the course, above-the-average
complexitiy tasks will also be dealt with, where utilization of the
cross-platform make system CMake will be used. Although ready-to-use
CMake files will be provided, it would be advantageous to learn
the basics of CMake.
As in all platforms, it is absolutely necessary to test your program,
so a CMake test and GoogleTest aspects will also be shortly discussed.

# The way of working with the course
It is a typical "learning by doing" course.
In the following the Linux operating system, specifically Ubuntu 18.04
is assumed.

In this very first course, you need to make your working directory.
(actually, I suggest something like
> mkdir SystemC/SCcourse/; cd SystemC/SCcourse/
)

## Initialize git for yourself
(To see the details, please take the course at
https://www.atlassian.com/git
)

> git init

This will initialize git for you. (creates a .git subdirectory, with
all its stuff.)

(if not, please install git:
> sudo apt-get install git

)

You can start with that immediately,
but it is adviced to make some customization, like

> git config --global user.name YourName

> git config --local user.email @You@mail.org

> git config --global alias.st status

> git config --global alias.ci commit

> git config --global alias.co checkout

Make a clone of the study directory
>git close lshttps://bitbucket.org/jvegh/sccourse
You will receive a new subdirectory, as requested, sccourse

You will work with this remote directory, so make a shortcut:
> git remote add origin https://bitbucket.org/jvegh/systemc-course

 > git co

If you receive a message that your brach is up-to date, you are OK
> cd sccourse

> ls

You will receive the list of your first files:
LICENSE.md  main.cpp  README.md



# Start with the good old "Hello World", the C++ way

As you did some years ago, write your first (purely C++) application
(see main.cpp in your directory)

> g++ main.
> ./a.out

Your very first C++ program will  compile and run

(if not, please install your C++:
> sudo apt install build-essential
)
